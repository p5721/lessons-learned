Spreadsheet for collecting a project's lessons-learned
===================

Set up for engineering projects, the spreadsheet allows you to diligently record essential information from lessons learned for use in future projects. This project arises from a need for peers across firms to more effectively plan together.  

The person organizing the work effort can use spreadsheet to capture observations during a project retrospective (lessons-learned) session with team members.  Actions taken from these observations improve future project performance.  

## Table of Contents

* [Usage](#usage) 
* [Installation](#installation) 
* [Uses](#uses) 
* [Needs](#needs) 
* [Contributing](#contributing) 
* [Credits](#credits) 
* [License](#license) 
* [Donations](#donations)


## Usage

This project involves using a Microsoft Excel spreadsheet to give structure to how a team remembers and addresses issues that arose during a project or project phase. 

<!--
Rough guide included: 
`inf-scdesigns-drawing_list_procedure-R001.docx`
-->

Spreadsheet tool:
`reg-scdesigns-lessons_learned-TEMPLATE-20170410.xlsm`

Tabs in the file cover the Impact rating table, the record of lessons learned, and the pick lists for workgroup and lesson type.     

1. Enter the project information in the Lessons Learned, comments in the column headers provide help

2. Enter the details for each lesson as your team describes it


## Installation 

1. Download the repository to where you intend to work  
2. Extract the project into the folder  


## Needs

None.  

## Requirements

 * Microsoft Excel (still need to test with other programs, likely to work with LibreOffice)

<!--
#### Debian
```bash
sudo apt install python3-feedparser
```

#### Fedora
```bash
sudo dnf install pandoc texlive-collection-context
```

#### Arch
```bash
sudo pacman -S pandoc texlive-core
```
-->
## Troubleshooting


## Contributing

Please do.  

## Credits

* Scott Cameron, ASTTBC

## License

Creative Commons Share and Share Alike, latest.

## Donations

TODO
<!--
We accept donations at the [VECTOR website](https://vectorradio.ca/donate/)
-->
